﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class footsteps_curves : MonoBehaviour
{
    public AK.Wwise.Event footevent;
    public AK.Wwise.Event waterevent;
    public AK.Wwise.Event slideevent;
    public AK.Wwise.CallbackFlags MyCallback; // Выбираем тип коллбэка
    public Animator animator;
    public GameObject footLeft;  // это объект левой ноги. перетаскиваем в компонент
    public GameObject footRight;
    public LayerMask lm;
    public bool PlayingLeft;
    public bool PlayingRight;
    public bool PlayingSlide;
    
    vThirdPersonInput tpInput;
    void Start()
    {
        tpInput = GetComponent<vThirdPersonInput>();
    }
    void FixedUpdate()
    {
       PlaysoundfootLeft();
       PlaysoundfootRight();
       PlaysoundSlide();
    }

    void PlaysoundSlide()
    {
        AkSoundEngine.SetRTPCValue("RTPC_ext_InputHorizontal", animator.GetFloat("InputHorizontal"), gameObject);
        AkSoundEngine.SetRTPCValue("RTPC_ext_Magnitude", tpInput.cc.inputMagnitude, gameObject);
        if (animator.GetFloat("InputHorizontal") < -0.1f && !PlayingSlide)
        {
            PlayingSlide = true;
            Debug.Log(animator.GetFloat("InputHorizontal"));
            slideevent.Post(gameObject, MyCallback, MonitoringCallback);
        }
        else if (animator.GetFloat("InputHorizontal") > 0.1f && !PlayingSlide)
        {
            PlayingSlide = true;
            Debug.Log(animator.GetFloat("InputHorizontal"));
            slideevent.Post(gameObject, MyCallback, MonitoringCallback);
        }
        
      
    }

    void PlaysoundfootLeft()
    {
        
        float footfallCurve = animator.GetFloat("footLeft");
        if (footfallCurve > 0.01 && !PlayingLeft)
        {
            PlayingLeft = true;
            Playfootstep(footLeft, footfallCurve);
            // footevent.Post(gameObject, MyCallback, MonitoringCallback) // Вызов коллбэка
        }
        else if (footfallCurve == 0)
        {
            PlayingLeft = false;
        }
    }
    
    void PlaysoundfootRight()
    {
         
        float footfallCurve = animator.GetFloat("footRight");
        if (footfallCurve > 0.01 && !PlayingRight)
        {
            
            PlayingRight = true;
            Playfootstep(footRight, footfallCurve);
        }
        else if (footfallCurve == 0)
        {
            PlayingRight = false;
        }

    }

    void Playfootstep(GameObject footObject, float curve)
    {
        if (Physics.Raycast(footObject.transform.position, Vector3.down, out RaycastHit hit, 0.3f, lm)) // запускаем рейкаст из объекта нужной ноги вниз
        {
            
            Debug.Log(hit.transform.gameObject.GetComponent<MeshRenderer>().tag);
            
            AkSoundEngine.SetRTPCValue("RTPC_ext_Magnitude", tpInput.cc.inputMagnitude, footObject);
            AkSoundEngine.SetRTPCValue("RTPC_ext_InputVertical", animator.GetFloat("InputVertical"), footObject);
            if (tpInput.cc.isSprinting)
            {
                AkSoundEngine.SetSwitch("locomotion", "running", footObject);
            }
            else
            {
                AkSoundEngine.SetRTPCValue("RTPC_ext_FootFall", curve*100, footObject);
                AkSoundEngine.SetSwitch("locomotion", "walking", footObject);
                if (curve > 0.04f)
                {
                    AkSoundEngine.SetSwitch("walk_scuff", "walk", footObject); 
                
                }
                else
                {
                   AkSoundEngine.SetSwitch("walk_scuff", "scuff", footObject);
                  
                } 
                
            } 
            
            AkSoundEngine.SetSwitch("surface", hit.collider.tag, footObject);
            if (hit.collider.tag == "water") waterevent.Post(footObject);
            footevent.Post(footObject);
        }
        
    }
    void MonitoringCallback(object cookie, AkCallbackType type, AkCallbackInfo info)
    {
        PlayingSlide = false;
        
    }
}