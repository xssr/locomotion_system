﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class cloth_sound : MonoBehaviour
{
    public AK.Wwise.Event clothevent;
    public vThirdPersonInput tpInput;
    public Collider clothobject;
    // Start is called before the first frame update
    

    // Update is called once per frame
   

    private void OnTriggerEnter(Collider other)
    {
        if (other == clothobject)
        {
            AkSoundEngine.SetRTPCValue("RTPC_ext_Magnitude", tpInput.cc.inputMagnitude, gameObject);
            if (tpInput.cc.isSprinting)
            {
                AkSoundEngine.SetSwitch("locomotion", "running", gameObject);
            }
            else AkSoundEngine.SetSwitch("locomotion", "walking", gameObject);
            clothevent.Post(gameObject);
           
        }
    }
}
