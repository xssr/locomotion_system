﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
using UnityEditor;

public class breath_controller : MonoBehaviour
{
    public AK.Wwise.Event breathevent;
    public AK.Wwise.Event idleevent;
    public AK.Wwise.Event walkingevent;
    public AK.Wwise.Event runningevent;
    public AK.Wwise.Event jumpevent;
    public AK.Wwise.Event landlowevent;
    
    
    public vThirdPersonController tpController;
    public bool isRunning = false;
    public bool isWalking = false;
    public bool isIdle = false;
    public AK.Wwise.CallbackFlags MyCallbackFlags = null;
    
    
    // Start is called before the first frame update
    void Start()
    {
        breathevent.Post(gameObject, MyCallbackFlags, MonitoringCallback);
    }

    void MonitoringCallback (object cookie, AkCallbackType type, AkCallbackInfo info)
    {
        if (type == AkCallbackType.AK_Marker)
        {
            var markerInfo = info as AkMarkerCallbackInfo;
            if (markerInfo != null)
            {
                Debug.Log(markerInfo.strLabel);
                AkSoundEngine.SetSwitch("breath_type", markerInfo.strLabel, gameObject);
            }
        }
    }
    void JumpFunction()
    {
        jumpevent.Post(gameObject);
    }

    void LandLowFunction()
    {
        landlowevent.Post(gameObject);
        jumpevent.Stop(gameObject);
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        AkSoundEngine.SetRTPCValue("RTPC_ext_stamina", tpController.currentStamina /2, gameObject );
        //Debug.Log(tpController.currentStamina);
        if (tpController.isGrounded && tpController.isSprinting && !isRunning)
        {
            runningevent.Post(gameObject);//, (uint) AkCallbackType.AK_EndOfEvent, SprintCallback);
            isRunning = true;
            isWalking = false;
            walkingevent.Stop(gameObject);
        }
        else if (!tpController.isSprinting && isRunning)
        {
            runningevent.Stop(gameObject);
            isRunning = false; 
            
        }
        
       else if (tpController.isGrounded && tpController.inputMagnitude > 0.1f && !isWalking &&!tpController.isSprinting )
        {
            walkingevent.Post(gameObject);
            isWalking = true;
            
        }
        else if (tpController.isGrounded && !tpController.isSprinting && tpController.inputMagnitude < 0.1f && isWalking)
        {
            walkingevent.Stop(gameObject);
            isWalking = false;  
            
        }
       else if (tpController.isGrounded && !tpController.isSprinting && !isWalking && !isIdle)
        {
            idleevent.Post(gameObject);
            isIdle = true;
            
        }
        else if (isIdle && tpController.inputMagnitude > 0.1f)
        {
            isIdle = false;
            
        }
    }

    
}
